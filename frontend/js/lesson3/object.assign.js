/*
  Object Assign, Spread and Rest Operator
  Docs:
    https://developer.mozilla.org/uk/docs/Web/JavaScript/Reference/Global_Objects/Object/assign
*/

// Object.assign
// syntax: Object.assign(target, ...sources)

/*
let DataObj = {
  data1: 'data1',
  data2: 'data2'
};

let DataObj2 = {
  data3: 'data3',
  data4: 'data4'
};

let firstAssign = Object.assign(DataObj, DataObj2);
console.log('firstAssign', firstAssign);
console.log('DataObj', DataObj);
// Изменяем значение исходного обьекта и проверяем значения обеих
DataObj.data5 = 'data5';
console.log('DataObj', DataObj);
console.log('firstAssign', firstAssign);

// IMMUTABLE ASSIGN

let secondAssign = Object.assign({}, DataObj, DataObj2 );
console.log( 'secondAssign', secondAssign );
DataObj.data6 = 'data6';
console.log('DataObj - secondAssign', DataObj);
console.log('secondAssign', secondAssign);

let FunctionalObj = {
  x: () => {
    console.log('some important stuff');
  },
  y: {
    a: 'a',
    b: 'b',
    c: 'c'
  }
};

FunctionalObj.x();

let thirdAssign = Object.assign({}, FunctionalObj);
console.log( thirdAssign );
thirdAssign.x();

// convert to obj
var v1 = 'abc';
var v2 = true;
var v3 = 10;
var v4 = Symbol('foo');

var obj = Object.assign({}, v1, null, v2, undefined, v3, v4);

console.log(obj);

var obj = {
  foo: 1,
  get bar() {
    return 2;
  }
};

var copy = Object.assign({}, obj);
console.log(copy);


// REST Operator

// in Function ->
function RestTest(a, b, ...props){
  console.log('a:', a, 'b', b, 'props', ...props);
}
var args = [0, 1, 2, 4, 5];
RestTest.apply(null, args);


// In array:
var iterableObj = ['i','t','e'];
var x = [ '4', 'five', 6, ...iterableObj];
console.log( 'rest in array:', x);

var arr = [1, 2, 3];
var arr2 = [...arr]; // like arr.slice()
arr2.push(4);

// concat arrays: old way;
var arr1 = [0, 1, 2];
var arr2 = [3, 4, 5];
// Append all items from arr2 onto arr1
arr1 = arr1.concat(arr2);
// new way:
arr1 = [...arr1, ...arr2];


// In obj
let objClone = { ...obj, ...DataObj };
console.log( 'objClone', objClone );

// let { x, y, ...z } = { x: 1, y: 2, a: 3, b: 4 };
// console.log(x); // 1
// console.log(y); // 2
// console.log(z); // { a: 3, b: 4 }

var sources = [{a: "A"}, {b: "B"}, {c: "C"}];
options = Object.assign.apply(Object, [{}].concat(sources));
console.log( options );
*/
