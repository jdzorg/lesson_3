( function MyApp(){
  // console.log('init function');
  // https://www.json-generator.com/#
  var fetchedData = fetch('http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2').then(function(response) {
    return response.json();
  }).then(data => {
    renderInterface(data);
  });

}());

function renderInterface( data ){
  // console.log('do some stuff with data:', data);
}
